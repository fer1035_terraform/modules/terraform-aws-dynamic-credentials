resource "tfe_variable_set" "variable_set" {
  name         = "${var.tfc_organization_name} - ${var.tfc_project_name} - ${var.tfc_workspace_name}"
  description  = "Variable Set for ${var.tfc_project_name}/${var.tfc_workspace_name} in the ${var.tfc_organization_name} organization."
  organization = var.tfc_organization_name
  global       = false
  # priority   = false
}

resource "tfe_variable" "provider_auth" {
  key             = "TFC_AWS_PROVIDER_AUTH"
  value           = "true"
  category        = "env"
  description     = "Use dynamic credentials for the AWS provider."
  variable_set_id = tfe_variable_set.variable_set.id
  hcl             = false
  sensitive       = false
}

resource "tfe_variable" "run_role_arn" {
  key             = "TFC_AWS_RUN_ROLE_ARN"
  value           = aws_iam_role.tfc_role.arn
  category        = "env"
  description     = "The role that AWS will use to manage resources with dynamic credentials."
  variable_set_id = tfe_variable_set.variable_set.id
  hcl             = false
  sensitive       = false
}
