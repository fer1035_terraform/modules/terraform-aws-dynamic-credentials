variable "tfc_hostname" {
  description = "TFC hostname"
  type        = string
  default     = "app.terraform.io"
}

variable "tfc_aws_audience" {
  description = "TFC AWS audience"
  type        = string
  default     = "aws.workload.identity"
}

variable "tfc_organization_name" {
  description = "TFC organization name"
  type        = string
}

variable "tfc_project_name" {
  description = "TFC project name"
  type        = string
  default     = "*"
}

variable "tfc_workspace_name" {
  description = "TFC workspace name"
  type        = string
  default     = "*"
}

variable "tfc_run_phase" {
  description = "TFC run phase"
  type        = string
  default     = "plan"
}

variable "aws_policy_actions" {
  description = "AWS policy actions"
  type        = list(string)
  default     = ["sts:GetCallerIdentity"]
}

variable "aws_policy_resources" {
  description = "AWS policy resources"
  type        = list(string)
  default     = ["*"]
}
