provider "aws" {
  region = "us-east-1"
}

module "credentials" {
  source  = "app.terraform.io/fer1035/dynamic-credentials/aws"
  version = "1.0.6"

  tfc_hostname          = "app.terraform.io"
  tfc_aws_audience      = "aws.workload.identity"
  tfc_organization_name = "my-org"
  tfc_project_name      = "my-project"
  tfc_workspace_name    = "my-workspace"
  tfc_run_phase         = "plan"

  aws_policy_actions = [
    "sts:GetCallerIdentity",
    "s3:*"
  ]

  aws_policy_resources = [
    "*"
  ]
}

output "variable_set_name" {
  value = module.credentials.variable_set_name
}
