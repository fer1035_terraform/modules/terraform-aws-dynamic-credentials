provider "aws" {
  region = "us-east-1"
}

module "credentials" {
  source = "app.terraform.io/fer1035/dynamic-credentials/aws"

  tfc_organization_name = "my-org"
}

output "variable_set_name" {
  value = module.credentials.variable_set_name
}
