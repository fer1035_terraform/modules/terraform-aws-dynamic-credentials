# terraform-aws-dynamic-credentials

A Terraform module to manage OIDC dynamic provider credentials in AWS.

## Overview

Dynamic provider credentials allow Terraform to perform actions inside Cloud environments without authenticating and authorizing using static credentials such as Access Key ID and Secret Access Key. When Terraform tries to gain access, the environment checks its Organization, Project, and Workspace to ensure that it has the necessary privileges. If it does, it will be assigned a temporary credential which will define what it's allowed to do in the environment.  

The module creates a Variable Set in Terraform Cloud that can be targeted to Projects and Workspaces.

![Workflow diagram](https://gitlab.com/fer1035_terraform/modules/terraform-aws-dynamic-credentials/-/raw/main/images/workflow.png)

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.0 |
| <a name="provider_tfe"></a> [tfe](#provider\_tfe) | n/a |
| <a name="provider_tls"></a> [tls](#provider\_tls) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_openid_connect_provider.tfc_provider](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |
| [aws_iam_policy.tfc_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.tfc_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.tfc_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [tfe_variable.provider_auth](https://registry.terraform.io/providers/hashicorp/tfe/latest/docs/resources/variable) | resource |
| [tfe_variable.run_role_arn](https://registry.terraform.io/providers/hashicorp/tfe/latest/docs/resources/variable) | resource |
| [tfe_variable_set.variable_set](https://registry.terraform.io/providers/hashicorp/tfe/latest/docs/resources/variable_set) | resource |
| [tls_certificate.tfc_certificate](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/data-sources/certificate) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_policy_actions"></a> [aws\_policy\_actions](#input\_aws\_policy\_actions) | AWS policy actions | `list(string)` | <pre>[<br>  "sts:GetCallerIdentity"<br>]</pre> | no |
| <a name="input_aws_policy_resources"></a> [aws\_policy\_resources](#input\_aws\_policy\_resources) | AWS policy resources | `list(string)` | <pre>[<br>  "*"<br>]</pre> | no |
| <a name="input_tfc_aws_audience"></a> [tfc\_aws\_audience](#input\_tfc\_aws\_audience) | TFC AWS audience | `string` | `"aws.workload.identity"` | no |
| <a name="input_tfc_hostname"></a> [tfc\_hostname](#input\_tfc\_hostname) | TFC hostname | `string` | `"app.terraform.io"` | no |
| <a name="input_tfc_organization_name"></a> [tfc\_organization\_name](#input\_tfc\_organization\_name) | TFC organization name | `string` | n/a | yes |
| <a name="input_tfc_project_name"></a> [tfc\_project\_name](#input\_tfc\_project\_name) | TFC project name | `string` | `"*"` | no |
| <a name="input_tfc_run_phase"></a> [tfc\_run\_phase](#input\_tfc\_run\_phase) | TFC run phase | `string` | `"plan"` | no |
| <a name="input_tfc_workspace_name"></a> [tfc\_workspace\_name](#input\_tfc\_workspace\_name) | TFC workspace name | `string` | `"*"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_openid_claims"></a> [openid\_claims](#output\_openid\_claims) | OpenID Claims for trust relationship |
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | ARN for trust relationship role |
| <a name="output_variable_set_name"></a> [variable\_set\_name](#output\_variable\_set\_name) | Variable Set name |
